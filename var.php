<?php
/**
 * Client side configuration.
 * @copyright Copyright (C) 2011 ultranoir
 */
require_once('conf.inc.php');


//	========================================================================================================================
//	WRITE JAVASCRIPT CONFIGURATION
//	========================================================================================================================
?>
//<![CDATA[
<?php
foreach($confPhpJs as $k=>$v) {
	echo "var $k = '$v';
";
}
?>