<?php
/**
 * Project configuration.
 */

//	DEV MODE
//	========
// Enable all error logging while in development
ini_set('display_errors', 'on');
error_reporting(E_ALL);

//ABSOLUTES & RELATIVES PATH
//	=======================
define('DOC_ROOT'					, dirname(__FILE__).'/');
define('DOC_ROOT_DATA'				, DOC_ROOT.'data/');
define('DOC_ROOT_CONTENT'			, DOC_ROOT.'content/');
define('DOC_ROOT_INCLUDE'			, DOC_ROOT.'include/');
define('DOC_ROOT_LANGUE'			, DOC_ROOT.'langue/');
define('DOC_ROOT_CSS'				, DOC_ROOT.'css/');
define('DOC_ROOT_JS'				, DOC_ROOT.'js/');
define('ERR_404'					, DOC_ROOT.'404.html');
define('_MODE_TEMPLATE',                     false);

//CLIENT & SERVER SIDE CONFIGURATION
//==================================
$confPhpJs['WEB_ROOT']				= '/GIT/www.htmlready-light.com/';
$confPhpJs['WEB_ROOT_DATA']			= $confPhpJs['WEB_ROOT'].'data/';
$confPhpJs['WEB_ROOT_COMPONENT']	= $confPhpJs['WEB_ROOT'].'component/';
$confPhpJs['WEB_ROOT_CSS']			= $confPhpJs['WEB_ROOT'].'css/';
$confPhpJs['WEB_ROOT_JS']			= $confPhpJs['WEB_ROOT'].'js/';
define('REQUEST', trim(str_replace($confPhpJs['WEB_ROOT'], '', $_SERVER['REQUEST_URI']), '/'));

//USER-BASED CONFIGURATION
//========================
$confPhpJs['IP_CLIENT']				= $_SERVER['REMOTE_ADDR'];
if(isset($_SERVER['HTTP_HOST'])) {
	$confPhpJs['HTTP']					= 'http://'.$_SERVER['HTTP_HOST'].$confPhpJs['WEB_ROOT'];
	$confPhpJs['HTTP_DATA']				= 'http://'.$_SERVER['HTTP_HOST'].$confPhpJs['WEB_ROOT_DATA'];
	define('URI'						, 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
}

//MISC
//========
$confPhpJs['SHOW_DEBUG']					= false;

//	DEFINE SERVER SIDE CONFIGURATION
//	=================================
foreach($confPhpJs as $k=>$v) {
	eval('define(\''.$k.'\', \''.$v.'\');');
}
?>