<?php
/**
 * Dispatch request & centralized actions
 */
require_once 'conf.inc.php';
require_once DOC_ROOT.'vendor/categorizr/categorizr.php';
//DISPATCH BASED ON REQUEST
//	======================
$PATH = 'maquette';
if (_MODE_TEMPLATE) $PATH = 'template';
$TPL = 'home';
if ($_GET) {
     $TPL = $_GET['page'];
}
//GET FILES
//=========
//CSS
$css = 'tpl.'.$TPL.'.css';
//JS
$js = 'tpl.'.$TPL.'.js';
//var_dump($args, $PATH, $TPL);

//GET SOURCES
//	========
//for ajax request
if (isset($_GET['tpl'])) {
     ob_start();
     $viewFile = DOC_ROOT . $PATH . '/' . $TPL . '.inc.php';
     if (file_exists($viewFile))
          require_once($viewFile);
     echo json_encode(array('css' => WEB_ROOT_CSS . $css, 'html' => ob_get_clean()));
}
else {
     require_once(DOC_ROOT_INCLUDE . 'com.builder.inc.php');
     require_once(DOC_ROOT_INCLUDE . 'com.header.inc.php');
     require_once(DOC_ROOT_INCLUDE . 'com.mainnav.inc.php');
     if ($PATH && $TPL) {
          $viewFile = DOC_ROOT . $PATH . '/' . $TPL . '.inc.php';
          if (file_exists($viewFile))
               require_once($viewFile);
     }
     require_once(DOC_ROOT_INCLUDE . 'com.footer.inc.php');
     require_once(DOC_ROOT_INCLUDE . 'com.manager.inc.php');
}
?>