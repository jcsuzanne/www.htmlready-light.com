<!doctype html><!-- HTMLREADY Framework -->
<!--[if IE]><![endif]-->
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="fr"> <!--<![endif]-->
<head>
	<title>HtmlReady Light</title>
	<meta charset="utf-8">
	<meta name="description" content="" />
	<?php if(isTablet()) { ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<?php } elseif(isMobile()) { ?>
		<meta name="viewport" content="width=device-width, initial-scale=0.0, maximum-scale=1.0" />
	<?php } else { ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" /><!-- Mobile viewport optimized: j.mp/bplateviewport -->
	<?php } ?>
	<!--[if IE]> <meta http-equiv="imagetoolbar" content="no" /> <![endif]-->
	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<link rel="canonical" href="" /><!-- unique url for google -->
	<link rel="shortcut icon" href="<?php echo WEB_ROOT_COMPONENT ?>image/favicon.ico" /><!-- favicon and apple touch icon -->
<!--	<link rel="apple-touch-icon" href="<?php echo WEB_ROOT_COMPONENT ?>image/apple-touch-icon.png" /> favicon and apple touch icon -->
<!--	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo WEB_ROOT_COMPONENT ?>image/apple-touch-icon-72x72-precomposed.png" /> favicon and apple touch icon -->
<!--	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo WEB_ROOT_COMPONENT ?>image/apple-touch-icon-114x114-precomposed.png" /> favicon and apple touch icon -->
	<link rel="stylesheet" href="<?php echo WEB_ROOT_CSS ?>master.style.php" type="text/css" media="screen" />
     <link rel="stylesheet" href="<?php echo WEB_ROOT_CSS ?>template.style.php?template=<?php echo $TPL; ?>" type="text/css" media="screen" id="css-template" />
	<!--[if IE 6]> <link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT_CSS ?>adapt.ie6.css" media="screen" /> <![endif]-->
	<!--[if lte IE 8]> <link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT_CSS ?>adapt.ie.css" media="screen" /> <![endif]-->
	<!--Modernizr which enables HTML5 elements & feature detects-->
	<script type="text/javascript" src="<?php echo HTTP ?>var.php"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT_JS ?>master.lib.php"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT_JS ?>template.js.php?template=<?php echo $TPL; ?>"></script>
</head>
<body role="document">
<!--START MASTER-->
<div id="master">