/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 *
 * Requires: 1.2.2+
 */
(function(a){function d(b){var c=b||window.event,d=[].slice.call(arguments,1),e=0,f=!0,g=0,h=0;return b=a.event.fix(c),b.type="mousewheel",c.wheelDelta&&(e=c.wheelDelta/120),c.detail&&(e=-c.detail/3),h=e,c.axis!==undefined&&c.axis===c.HORIZONTAL_AXIS&&(h=0,g=-1*e),c.wheelDeltaY!==undefined&&(h=c.wheelDeltaY/120),c.wheelDeltaX!==undefined&&(g=-1*c.wheelDeltaX/120),d.unshift(b,e,g,h),(a.event.dispatch||a.event.handle).apply(this,d)}var b=["DOMMouseScroll","mousewheel"];if(a.event.fixHooks)for(var c=b.length;c;)a.event.fixHooks[b[--c]]=a.event.mouseHooks;a.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var a=b.length;a;)this.addEventListener(b[--a],d,!1);else this.onmousewheel=d},teardown:function(){if(this.removeEventListener)for(var a=b.length;a;)this.removeEventListener(b[--a],d,!1);else this.onmousewheel=null}},a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})})(jQuery)


/* function to fix the -10000 pixel limit of jquery.animate */
$.fx.prototype.cur = function(){
    if ( this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null) ) {
      return this.elem[ this.prop ];
    }
    var r = parseFloat( jQuery.css( this.elem, this.prop ) );
    return typeof r == 'undefined' ? 0 : r;
}


/*
 * Perner easing functions
 * Codehinting for JQuery easing functions
 * http://gsgd.co.uk/sandbox/jquery/easing/
 *
 */
var Quad={};var Cubic={};var Quart={};var Quint={};var Sine={};var Expo={};var Circ={};var Bounce={};var Elastic={};var Linear={};var Back={};Linear.easeNone="linear";Quad.easeIn="easeInQuad";Quad.easeOut="easeOutQuad";Quad.easeInOut="easeInOutQuad";Cubic.easeIn="easeInCubic";Cubic.easeOut="easeOutCubic";Cubic.easeInOut="easeInOutCubic";Quart.easeIn="easeInQuart";Quart.easeOut="easeOutQuart";Quart.easeInOut="easeInOutQuart";Quint.easeIn="easeInQuint";Quint.easeOut="easeOutQuint";Quint.easeInOut="easeInOutQuint";Sine.easeIn="easeInSine";Sine.easeOut="easeOutSine";Sine.easeInOut="easeInOutSine";Expo.easeIn="easeInExpo";Expo.easeOut="easeOutExpo";Expo.easeInOut="easeInOutExpo";Circ.easeIn="easeInCirc";Circ.easeOut="easeOutCirc";Circ.easeInOut="easeInOutCirc";Bounce.easeIn="easeInBounce";Bounce.easeOut="easeOutBounce";Bounce.easeInOut="easeInOutBounce";Elastic.easeIn="easeInElastic";Elastic.easeOut="easeOutElastic";


/*
 * Returning Scroll dimensions
 * http://api.jquery.com/outerHeight/
 *
 */
jQuery.fn.outerScrollWidth=function(includeMargin){var element=this[0];var jElement=$(element);var totalWidth=element.scrollWidth;totalWidth+=jElement.outerWidth(includeMargin)-jElement.innerWidth();return totalWidth};
jQuery.fn.outerScrollHeight=function(includeMargin){var element=this[0];var jElement=$(element);var totalHeight=element.scrollHeight;totalHeight+=jElement.outerHeight(includeMargin)-jElement.innerHeight();return totalHeight};


/**
 * Active the animate function on background-position
 * @author Alexander Farkas
 * v. 1.22
 */
(function($){if(!document.defaultView||!document.defaultView.getComputedStyle){var oldCurCSS=$.curCSS;$.curCSS=function(elem,name,force){if(name==='background-position'){name='backgroundPosition'}if(name!=='backgroundPosition'||!elem.currentStyle||elem.currentStyle[name]){return oldCurCSS.apply(this,arguments)}var style=elem.style;if(!force&&style&&style[name]){return style[name]}return oldCurCSS(elem,'backgroundPositionX',force)+' '+oldCurCSS(elem,'backgroundPositionY',force)}}var oldAnim=$.fn.animate;$.fn.animate=function(prop){if('background-position'in prop){prop.backgroundPosition=prop['background-position'];delete prop['background-position']}if('backgroundPosition'in prop){prop.backgroundPosition='('+prop.backgroundPosition}return oldAnim.apply(this,arguments)};function toArray(strg){strg=strg.replace(/left|top/g,'0px');strg=strg.replace(/right|bottom/g,'100%');strg=strg.replace(/([0-9\.]+)(\s|\)|$)/g,"$1px$2");var res=strg.match(/(-?[0-9\.]+)(px|\%|em|pt)\s(-?[0-9\.]+)(px|\%|em|pt)/);return[parseFloat(res[1],10),res[2],parseFloat(res[3],10),res[4]]}$.fx.step.backgroundPosition=function(fx){if(!fx.bgPosReady){var start=$.curCSS(fx.elem,'backgroundPosition');if(!start){start='0px 0px'}start=toArray(start);fx.start=[start[0],start[2]];var end=toArray(fx.end);fx.end=[end[0],end[2]];fx.unit=[end[1],end[3]];fx.bgPosReady=true}var nowPosX=[];nowPosX[0]=((fx.end[0]-fx.start[0])*fx.pos)+fx.start[0]+fx.unit[0];nowPosX[1]=((fx.end[1]-fx.start[1])*fx.pos)+fx.start[1]+fx.unit[1];fx.elem.style.backgroundPosition=nowPosX[0]+' '+nowPosX[1]}})(jQuery);

/**
 * jQuery Timers
 * Modified from http://jquery.offput.ca/every/
 */
jQuery.fn.extend({everyTime:function(interval,label,fn,times,belay){return this.each(function(){jQuery.timer.add(this,interval,label,fn,times,belay)})},oneTime:function(interval,label,fn){return this.each(function(){jQuery.timer.add(this,interval,label,fn,1)})},killTime:function(label,fn){return this.each(function(){jQuery.timer.kill(this,label,fn)})},pauseTime:function(label,fn){return this.each(function(){jQuery.timer.pause(this,label,fn)})},resumeTime:function(label,fn){return this.each(function(){jQuery.timer.resume(this,label,fn)})},killAll:function(){return this.each(function(){jQuery.timer.kill_all(this)})},pauseAll:function(){return this.each(function(){jQuery.timer.pause_all(this)})},resumeAll:function(){return this.each(function(){jQuery.timer.resume_all(this)})},dumpAll:function(){return this.each(function(){jQuery.timer.list(this)})}});jQuery.extend({timer:{guid:1,global:{},regex:/^([0-9]+)\s*(.*s)?$/,powers:{'ms':1,'cs':10,'ds':100,'s':1000,'das':10000,'hs':100000,'ks':1000000},timeParse:function(value){if(value==undefined||value==null)return null;var result=this.regex.exec(jQuery.trim(value.toString()));if(result[2]){var num=parseInt(result[1],10);var mult=this.powers[result[2]]||1;return num*mult}else{return value}},add:function(element,interval,label,fn,times,belay){var counter=0;if(jQuery.isFunction(label)){if(!times)times=fn;fn=label;label=interval}interval=jQuery.timer.timeParse(interval);if(typeof interval!='number'||isNaN(interval)||interval<=0)return;if(times&&times.constructor!=Number){belay=!!times;times=0}times=times||0;belay=belay||false;if(!element.$timers)element.$timers={};if(!element.$timers[label])element.$timers[label]={};fn.$timerID=fn.$timerID||this.guid++;var handler=function(){if(belay&&this.inProgress)return;this.inProgress=true;if((++counter>times&&times!==0)||fn.call(element,counter)===false)jQuery.timer.kill(element,label,fn);this.inProgress=false};handler.$timerID=fn.$timerID;if(!element.$timers[label][fn.$timerID]){handler.$interval=interval;handler.$timer=window.setInterval(handler,interval);element.$timers[label][fn.$timerID]=handler}if(!this.global[label])this.global[label]=[];this.global[label].push(element)},resume:function(element,label,fn){var timers=element.$timers,ret;if(timers){if(!label){for(label in timers)this.resume(element,label,fn)}else if(timers[label]){if(fn){if(fn.$timerID){var handler=timers[label][fn.$timerID];if(!handler.$timer){handler.$timer=window.setInterval(handler,handler.$interval);timers[label][fn.$timerID]=handler}}}else{for(var fn in timers[label]){var handler=timers[label][fn];if(!handler.$timer){handler.$timer=window.setInterval(handler,handler.$interval);timers[label][fn]=handler}}}}}},pause:function(element,label,fn){var timers=element.$timers,ret;if(timers){if(!label){for(label in timers)this.pause(element,label,fn)}else if(timers[label]){if(fn){if(fn.$timerID){var handler=timers[label][fn.$timerID];window.clearInterval(handler.$timer);handler.$timer=null;timers[label][fn]=handler}}else{for(var fn in timers[label]){var handler=timers[label][fn];window.clearInterval(handler.$timer);handler.$timer=null;timers[label][fn]=handler}}}}},kill:function(element,label,fn){var timers=element.$timers,ret;if(timers){if(!label){for(label in timers)this.kill(element,label,fn)}else if(timers[label]){if(fn){if(fn.$timerID){var handler=timers[label][fn.$timerID];window.clearInterval(handler.$timer);delete timers[label][fn.$timerID]}}else{for(var fn in timers[label]){var handler=timers[label][fn];window.clearInterval(handler.$timer);delete timers[label][fn]}}for(ret in timers[label])break;if(!ret){ret=null;delete timers[label]}}for(ret in timers)break;if(!ret)element.$timers=null}},list:function(element){var timers=element.$timers;for(var label in timers){for(var fn in timers[label]){var handler=timers[label][fn];alert("Timer [ label:"+label+" | id:"+handler.$timerID+"]")}}},kill_all:function(element){var timers=element.$timers;for(var label in timers){jQuery.timer.kill(element,label)}},pause_all:function(element){var timers=element.$timers;for(var label in timers){jQuery.timer.pause(element,label)}},resume_all:function(element){var timers=element.$timers;for(var label in timers){jQuery.timer.resume(element,label)}}}});if(jQuery.browser.msie)jQuery(window).one("unload",function(){var global=jQuery.timer.global;for(var label in global){var els=global[label],i=els.length;while(--i)jQuery.timer.remove(els[i],label)}});


/**
 * bind new events // scrollstart // scrollstop
 * http://james.padolsey.com/demos/scrollevents/
 */
(function(){var special=jQuery.event.special,uid1='D'+(+new Date()),uid2='D'+(+new Date()+1);special.scrollstart={setup:function(){var timer,handler=function(evt){var _self=this,_args=arguments;if(timer){clearTimeout(timer)}else{evt.type='scrollstart';jQuery.event.handle.apply(_self,_args)}timer=setTimeout(function(){timer=null},special.scrollstop.latency)};jQuery(this).bind('scroll',handler).data(uid1,handler)},teardown:function(){jQuery(this).unbind('scroll',jQuery(this).data(uid1))}};special.scrollstop={latency:300,setup:function(){var timer,handler=function(evt){var _self=this,_args=arguments;if(timer){clearTimeout(timer)}timer=setTimeout(function(){timer=null;evt.type='scrollstop';jQuery.event.handle.apply(_self,_args)},special.scrollstop.latency)};jQuery(this).bind('scroll',handler).data(uid2,handler)},teardown:function(){jQuery(this).unbind('scroll',jQuery(this).data(uid2))}}})();

/**
 * http://stackoverflow.com/questions/1184624/convert-form-data-to-js-object-with-jquery
 */
$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

/**
 * http://paulgueller.com/2011/04/26/parse-the-querystring-with-jquery/
 */
jQuery.extend({
	parseQuerystring: function(){
		var nvpair = {};
		if(window.location.href.indexOf('?')!=-1) {
			var qs = window.location.search.replace('?', '');
			var pairs = qs.split('&');
			$.each(pairs, function(i, v){
				var pair = v.split('=');
				nvpair[pair[0]] = pair[1];
			});
		}
		return nvpair;
	}
});