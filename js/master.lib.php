<?php
require_once '../conf.inc.php';
require_once '../splClassLoader.php';

$classLoader = new SplClassLoader('Assetic', __DIR__.'/../vendor/assetic');
$classLoader->register();

use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
// use Assetic\Filter\Yui\JsCompressorFilter as YuiCompressorFilter;

$collection = array(
    new FileAsset(__DIR__.'/../js/cdn/modernizr/source.js'),
    new FileAsset(__DIR__.'/../js/cdn/jquery/source.js'),
    new FileAsset(__DIR__.'/../js/cdn/jquery-ui/source.js'),
    new FileAsset(__DIR__.'/../js/cdn/jquery-enhance/source.js'),
    new FileAsset(__DIR__.'/../js/com.core.js')
);

$js = new AssetCollection($collection);
header('Content-Type: text/javascript');
echo $js->dump();
?>