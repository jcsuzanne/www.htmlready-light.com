<?php
require_once '../conf.inc.php';
require_once '../splClassLoader.php';

$classLoader = new SplClassLoader('Assetic', __DIR__.'/../vendor/assetic');
$classLoader->register();

$classLoader = new SplClassLoader('Symfony', __DIR__.'/../vendor/symfony');
$classLoader->register();


use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\Asset\GlobAsset;
use Assetic\Filter\LessphpFilter;
use Assetic\Filter\CssRewriteFilter;

$css = new AssetCollection(array(
    new FileAsset(__DIR__.'/../css/cdn/com.reset.css'),
    new FileAsset(__DIR__.'/../css/cdn/com.utils.css'),
    new FileAsset(__DIR__.'/../css/cdn/lib.gcolumns.css'),
    new GlobAsset(__DIR__.'/../less/com.*.less', array(new LessphpFilter(), new CssRewriteFilter())),
    new GlobAsset(__DIR__.'/../less/mod.*.less', array(new LessphpFilter(), new CssRewriteFilter()))
));
header('Content-Type: text/css');
echo $css->dump();
?>
