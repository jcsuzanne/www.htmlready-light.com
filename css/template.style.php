<?php
require_once '../conf.inc.php';
require_once '../splClassLoader.php';

$classLoader = new SplClassLoader('Assetic', __DIR__.'/../vendor/assetic');
$classLoader->register();

$classLoader = new SplClassLoader('Symfony', __DIR__.'/../vendor/symfony');
$classLoader->register();

use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\Asset\GlobAsset;
use Assetic\Filter\LessphpFilter;
use Assetic\Filter\CssRewriteFilter;

$template = '';
if (isset($_GET['template'])) {
     $template = $_GET['template'];
     $css = new AssetCollection(array(
     new FileAsset(__DIR__ . '/../less/tpl.' . $template . '.less', array(new LessphpFilter(), new CssRewriteFilter()))
     ));
}
header('Content-Type: text/css');
if (isset($_GET['template'])) echo $css->dump();
?>
